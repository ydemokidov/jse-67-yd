package com.t1.yd.tm.listener.system;

import com.t1.yd.tm.api.model.IListener;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.listener.AbstractListener;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Setter
@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "help";
    @NotNull
    public static final String ARGUMENT = "-h";
    @NotNull
    public static final String DESCRIPTION = "Show info about program";

    @NotNull
    private List<AbstractListener> listeners;

    @Autowired
    public ApplicationHelpListener(@NotNull final ITokenService tokenService,
                                   @NotNull final IPropertyService propertyService) {
        super(tokenService, propertyService);
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (String commandHelp : getHelpStrings()) {
            System.out.println(commandHelp);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    public List<String> getHelpStrings() {
        List<String> helpStrings = new ArrayList<>();
        for (final IListener listener : listeners) helpStrings.add(listener.toString());
        return helpStrings;
    }
}
