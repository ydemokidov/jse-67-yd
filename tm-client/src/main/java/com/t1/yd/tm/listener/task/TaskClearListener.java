package com.t1.yd.tm.listener.task;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.task.TaskClearRequest;
import com.t1.yd.tm.event.ConsoleEvent;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task_clear";
    @NotNull
    public static final String DESCRIPTION = "Clear tasks";

    @Autowired
    public TaskClearListener(@NotNull final ITokenService tokenService,
                             @NotNull final ITaskEndpoint taskEndpointClient) {
        super(tokenService, taskEndpointClient);
    }

    @Override
    @EventListener(condition = "@taskClearListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CLEAR TASKS]");
        @NotNull final TaskClearRequest request = new TaskClearRequest();
        request.setToken(getToken());
        getTaskEndpointClient().clearTasks(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
