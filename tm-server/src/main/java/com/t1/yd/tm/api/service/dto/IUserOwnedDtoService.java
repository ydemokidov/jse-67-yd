package com.t1.yd.tm.api.service.dto;

import com.t1.yd.tm.dto.model.AbstractUserOwnedEntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface IUserOwnedDtoService<E extends AbstractUserOwnedEntityDTO> extends IDtoService<E> {

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Sort sort);

    void clear(@NotNull String userId);

    E add(@NotNull String userId, @NotNull E entity);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @Nullable
    E findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeById(@NotNull String userId, @NotNull String id);

    boolean existsById(@NotNull String userId, @NotNull String id);

    long getSize(@NotNull String userId);

}
