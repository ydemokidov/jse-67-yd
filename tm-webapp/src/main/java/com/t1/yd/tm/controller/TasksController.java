package com.t1.yd.tm.controller;

import com.t1.yd.tm.api.service.ITaskService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TasksController {

    private final ITaskService taskService;

    @Autowired
    public TasksController(@NotNull final ITaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/tasks")
    public ModelAndView listTasks() throws Exception {
        return new ModelAndView("task-list", "tasks", taskService.findAll());
    }

}
