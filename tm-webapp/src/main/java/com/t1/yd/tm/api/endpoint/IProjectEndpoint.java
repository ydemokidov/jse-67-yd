package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.model.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    @NotNull
    List<ProjectDTO> findAll() throws Exception;

    @WebMethod
    @NotNull
    ProjectDTO update(@WebParam(name = "project", partName = "project") @NotNull ProjectDTO project) throws Exception;

    @WebMethod
    @Nullable
    ProjectDTO findById(@WebParam(name = "id", partName = "id") @NotNull String id) throws Exception;

    @WebMethod
    boolean existsById(@WebParam(name = "id", partName = "id") @NotNull String id) throws Exception;

    @WebMethod
    long count() throws Exception;

    @WebMethod
    void deleteById(@WebParam(name = "id", partName = "id") @NotNull String id) throws Exception;

    @WebMethod
    void delete(@WebParam(name = "project", partName = "project") @NotNull ProjectDTO project) throws Exception;

    void clear() throws Exception;

}
