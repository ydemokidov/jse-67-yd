package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.IProjectDTOService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebService;
import java.util.List;

@RestController
@NoArgsConstructor
@RequestMapping("/api/project")
@WebService(endpointInterface = "com.t1.yd.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    private IProjectDTOService projectDTOService;

    @Autowired
    public ProjectEndpoint(IProjectDTOService projectDTOService) {
        this.projectDTOService = projectDTOService;
    }

    @Override
    @GetMapping("/list")
    public @NotNull List<ProjectDTO> findAll() throws Exception {
        return projectDTOService.findAll();
    }

    @Override
    @PostMapping("/update")
    public @NotNull ProjectDTO update(@RequestBody @NotNull final ProjectDTO project) throws Exception {
        return projectDTOService.update(project);
    }

    @Override
    @GetMapping("/{id}")
    public @Nullable ProjectDTO findById(@PathVariable @NotNull final String id) throws Exception {
        return projectDTOService.findOneById(id);
    }

    @Override
    @GetMapping("/exists/{id}")
    public boolean existsById(@PathVariable @NotNull final String id) throws Exception {
        return projectDTOService.existsById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() throws Exception {
        return projectDTOService.count();
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable @NotNull final String id) throws Exception {
        projectDTOService.removeById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull final ProjectDTO project) throws Exception {
        projectDTOService.remove(project);
    }

    @Override
    public void clear() throws Exception {
        projectDTOService.clear();
    }

}
