package com.t1.yd.tm.api.service;

import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectDTOService {

    @NotNull
    ProjectDTO add(@NotNull ProjectDTO project) throws Exception;

    void clear() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @NotNull
    List<ProjectDTO> findAll() throws Exception;

    @Nullable
    ProjectDTO findOneById(@Nullable String id) throws Exception;

    int count() throws Exception;

    void remove(@Nullable ProjectDTO project) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @NotNull
    ProjectDTO update(@Nullable ProjectDTO project) throws Exception;

    @NotNull
    ProjectDTO create(@Nullable String name) throws Exception;

    @NotNull
    ProjectDTO create(@Nullable String name, @Nullable String description) throws Exception;

    void updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void changeProjectStatusById(@Nullable String id, @Nullable Status status) throws Exception;

}
