package com.t1.yd.tm.constant;

import org.jetbrains.annotations.NotNull;

public class TaskTestData {

    @NotNull
    public final static String TASK1_NAME = "TASK1";

    @NotNull
    public final static String TASK1_DESCRIPTION = "TASK1_DESC";

    @NotNull
    public final static String TASK2_NAME = "TASK2";

    @NotNull
    public final static String TASK2_DESCRIPTION = "TASK2_DESC";

    @NotNull
    public final static String TASK3_NAME = "TASK3";

    @NotNull
    public final static String TASK3_DESCRIPTION = "TASK3_DESC";

}
