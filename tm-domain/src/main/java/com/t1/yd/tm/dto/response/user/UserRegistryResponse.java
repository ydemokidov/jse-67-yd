package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.dto.model.UserDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final UserDTO userDTO) {
        super(userDTO);
    }

    public UserRegistryResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
