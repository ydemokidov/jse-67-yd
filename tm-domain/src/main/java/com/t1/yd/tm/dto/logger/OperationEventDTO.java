package com.t1.yd.tm.dto.logger;

import com.t1.yd.tm.enumerated.OperationType;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class OperationEventDTO {

    @NotNull
    private final OperationType type;

    @NotNull
    private final Object entity;
    private final long timestamp = System.currentTimeMillis();
    private String table;

    public OperationEventDTO(@NotNull final OperationType type, @NotNull final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
