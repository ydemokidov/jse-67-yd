package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.dto.model.UserDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable final UserDTO userDTO) {
        super(userDTO);
    }

    public UserUpdateProfileResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
